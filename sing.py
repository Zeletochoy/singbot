from subprocess import Popen, PIPE, DEVNULL
import requests
import time
import json

def sing(text, voice="Melodine"):
    url = "http://voxygen.fr/sites/all/modules/voxygen_voices/assets/proxy/index.php?method=redirect"
    res = requests.get(url, params={"ts": int(time.time()), "text": text, "voice": voice})
    p = Popen(["/usr/bin/cvlc", "--play-and-exit", "-"], stdin=PIPE, stdout=DEVNULL, stderr=DEVNULL)
    p.communicate(res.content)

def get_voices():
    url = "http://voxygen.fr/voices.json"
    res = requests.get(url)
    yml = json.loads(res.content.decode("utf-8"))
    voices = []
    for lang in yml["groups"]:
        if lang["code"] == "fr":
            for v in lang["voices"]:
                voices.append(v["name"])
            return voices
