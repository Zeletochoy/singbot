#! /usr/bin/env python3

import pypeul
import sing
import random

class SingBot(pypeul.IRC):
  random_voice = True
  voice_idx = 0
  voice_map = {}
  voices = []

  def on_ready(self):
    self.join("#yaka-sing")
    self.voices = sing.get_voices()
    random.shuffle(self.voices)

  def on_channel_message(self, umask, target, msg):
    if self.random_voice:
      if umask.user.nick not in self.voice_map:
        self.voice_map[umask.user.nick] = self.voices[self.voice_idx]
        self.voice_idx = (self.voice_idx + 1) % len(self.voices)
      sing.sing(msg, self.voice_map[umask.user.nick])
    else:
      sing.sing(msg)

if __name__ == "__main__":
  bot = SingBot()
  bot.connect('irc.rezosup.org', 6667)
  bot.ident('Singer')
  bot.set_reconnect(lambda x: x * 5)
  bot.run()
